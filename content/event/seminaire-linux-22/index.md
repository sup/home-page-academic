---
title: MicroPython for the IoT

event: 16th Fribourg Linux Seminar
event_url: https://www.meetup.com/fr-FR/Fribourg-Linux-Seminar/events/256776987/

location: Haute école d'ingénierie et d'architecture
address:
  street: Pérolles 80
  city: Fribourg
  region:
  postcode: 1700
  country: Switzerland

summary: MicroPython for the IoT.
abstract: >
  According to the 2018 survey of Stack Overflow, Python is one of the most
  loved programming languages and the most wanted one. Python is used in many
  world-class software companies as well as in high schools, colleges and
  universities. MicroPython is an implementation of the Python 3 programming
  language optimized for embedded systems. During this presentation,
  I will present MicroPython and its derivatives, and I will show you some
  typical use cases for MicroPython. In the second part, I will demonstrate
  how to install the system on a cheap microcontroller and during a live
  coding session, I will implement a functional IoT device.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2019-05-02T16:45:00+02:00
date_end: 2019-05-16T20:00:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate:

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

# links:
#   - icon: twitter
#     icon_pack: fab
#     name: Follow
#     url: https://twitter.com/georgecushen
# url_code: ''
url_pdf: https://gitlab.forge.hefr.ch/fribourg-linux-seminar/seminars/-/raw/master/19.05_handout_16th_seminar/02_Micropython_for_the_IoT.pdf
url_slides: https://docs.google.com/presentation/d/1UVzaTxRC5UpQ9UfxAlZjKM-TleHxzgGJe-b6MChBkDY/edit?usp=sharing
# url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ''

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
