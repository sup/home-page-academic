---
title: Developing inside a Container

event: 22nd Fribourg Linux Seminar
event_url: https://www.meetup.com/fr-FR/fribourg-linux-seminar/events/285077319/

location: Haute école d'ingénierie et d'architecture
address:
  street: Pérolles 80
  city: Fribourg
  region:
  postcode: 1700
  country: Switzerland

summary: Developing inside a Container.
abstract: >

  Nowadays, it is no longer necessary to demonstrate the benefits of containers
  for service production, and in this presentation, I will show other benefits
  that containers can bring to the development of software.
  
  I will start with a brief introduction, explain the mechanism behind the technology,
  and I will then present a couple of real-world use cases. I will show how development
  containers helped me to address these cases and at the end of the presentation,
  you will know enough about this technique to apply it to your projects and improve
  your productivity.

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
date: 2022-05-19T16:45:00+02:00
date_end: 2022-05-19T21:00:00+02:00
all_day: false

# Schedule page publish date (NOT talk date).
publishDate:

authors: []
tags: []

# Is this a featured talk? (true/false)
featured: false

image:
  caption: ''
  focal_point: Right

# links:
#   - icon: twitter
#     icon_pack: fab
#     name: Follow
#     url: https://twitter.com/georgecushen
# url_code: ''
url_pdf: https://gitlab.forge.hefr.ch/fribourg-linux-seminar/seminars/-/raw/master/22.05_handout_22nd_seminar/04_developing_inside_a_container.pdf
url_slides: https://docs.google.com/presentation/d/17sWR-ms7f339UXTwGb8yZiuMhHS5hTq7_h__V9aRptE/edit?usp=sharing
# url_video: ''

# Markdown Slides (optional).
#   Associate this talk with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ''

# Projects (optional).
#   Associate this post with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `projects = ["internal-project"]` references `content/project/deep-learning/index.md`.
#   Otherwise, set `projects = []`.
projects: []
---
