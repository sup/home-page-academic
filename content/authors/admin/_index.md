---
# Display name
title: Jacques Supcik

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Professor of Computer Science and Communication Systems

# Organizations/Affiliations to show in About widget
organizations:
  - name: Haute école d'ingénierie et d'architecture de Fribourg
    url: https://www.heia-fr.ch/en/

# Short bio (displayed in user profile at end of posts)
bio: Technology addict and horses lover.

# Interests to show in About widget
interests:
  - Programming languages
  - Compilers and Interpreters
  - Algorithmes and Data structures
  - Embedded Systems and IoT
  - Linux Operating Systems
  - Cloud Computing
  - Machine Learning

# Education to show in About widget
education:
  courses:
  - course: Doctor of Technical Science
    institution: ETH Zürich
    year: 1999
  - course: Dipl. Informatik-Ing.
    institution: ETH Zürich
    year: 1992
  - course: "Maturité Type C (scientific)"
    institution: Collège St-Michel, Fribourg
    year: 1987

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: 'mailto:jacques.supcik@hefr.ch'  # For a direct email link, use "mailto:test@example.org".
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/supcik
  - icon: github
    icon_pack: fab
    link: https://github.com/supcik
  - icon: gitlab
    icon_pack: fab
    link: https://gitlab.com/supcik
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/supcik/
  - icon: dev
    icon_pack: fab
    link: https://dev.to/supcik


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.toml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 'jacques.supcik@hefr.ch'

# Highlight the author in author lists? (true/false)
highlight_name: true
---

Jacques Supcik is a full time professor in the [Computer Science and
Communication
Systems](https://www.heia-fr.ch/en/education/bachelor/computer-science-and-communication-systems/)
faculty of the [Haute école d'ingénierie et
d'architecture](https://www.heia-fr.ch/) of Fribourg. In his free time,
he continues to write code, learn things and tinker with technology, but
once a week, he goes out with his horse for a long
[ride](https://www.facebook.com/centre.equestre.corminboeuf/) in the
forest.

His research activities are mainly in the field of embedded systems,
but he is also interested in programming in general, in programming languages and in compilers.
His favorite programming languages are [Go](https://golang.org/) and
[Python](https://www.python.org/). He also likes to code using Kotlin
and he is interrested in [Rust](https://www.rust-lang.org/)
and [Carbon](https://github.com/carbon-language/carbon-lang/blob/trunk/README.md).
He teaches computer programming, computer architecture,
operating systems and embedded systems at Bachelor and Master level.
