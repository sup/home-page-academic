---
# An instance of the Experience widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: experience

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Experience
subtitle:

# Date format for experience
#   Refer to https://wowchemy.com/docs/customization/#date-format
date_format: Jan 2006

# Experiences.
#   Add/remove as many `experience` items below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
experience:
  - title: "Professor"
    company: "Haute école d'ingénierie et d'architecture"
    company_url: "https://www.heia-fr.ch/"
    company_logo: heiafr_tower
    location: "Fribourg"
    date_start: "2011-09-01"
    date_end: ""
    description: |2-
      * Computer programming and Algorithms and Data structures lectures for Bachelor students.
      * Operating Systems and Concurrent Programming lectures for Bachelor students.
      * Computer Architecture and Embedded Systems lectures for Bachelor students.
      * Construction of Embedded Systems under Linux lectures for Master students.
      * Member of the Applied Research Institute for Smart and Secure Systems.
      * Organiser of the "Fribourg Linux Seminars"
      * Organiser of the "Google Developer Group Fribourg"

  - title: "Team Leader"
    company: "Swisscom (Suisse) SA"
    company_url: "https://www.swisscom.ch/"
    company_logo: swisscom
    location: "Zürich/Bern"
    date_start: "2007-01-01"
    date_end: "2013-09-30"
    description: |2-
      * Head of the "Security Solution Sysadmin" team (sites of Bern and Zürich).
      * Operation of the Swisscom PKI, recognized according to the Swiss Certification Law (SCSE/ZertES).
      * Operation of 200+ UNIX and GNU/Linux servers across multiple sites, with 99.999% availability for the most critical
      services.
      * Optimization and improvement of service quality through the establishment of the "DevOps" culture in the team.

  - title: "Team leader and senior developer"
    company: "Swisscom (Suisse) SA"
    company_url: "https://www.swisscom.ch/"
    company_logo: swisscom

    location: "Zürich/Bern"
    date_start: "1999-06-01"
    date_end: "2007-01-31"
    description: |2-
      * Development of the "Swisscom Digital Certificate Service" (Public-Key Infrastructure).
      * Design and implementation of web portals (Internet and Extranet).
      * Design of the end-to-end internal monitoring system using Nagios.
      * Management and development of several projects to improve IP-Plus Internet services.

design:
  columns: '2'
---
