---
# An instance of the devto widget.
widget: devto

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 60

title: Recent Posts
subtitle:

content:
  username: "supcik"
  count: 5

design:
  # Choose a view for the listings:
  view: compact
  columns: '2'
---
