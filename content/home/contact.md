---
# An instance of the Contact widget.
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true

  # Contact details (edit or remove options as required)
  email: jacques.supcik@hefr.ch
  phone: +41 26 429 67 52
  address:
    street: Pérolles 80
    city: Fribourg
    postcode: '1700'
    country: Switzerland
    country_code: CH
  coordinates:
    latitude: '46.7926'
    longitude: '7.15993'
  directions: Enter building C and take the stairs (or the elevator) to office C10.05 on first floor
  contact_links:
    - icon: twitter
      icon_pack: fab
      name: DM Me
      link: 'https://twitter.com/supcik'
    - icon: telegram
      icon_pack: fab
      name: Chat on telegram
      link: 'http://telegram.me/supcik'

design:
  columns: '2'
---
