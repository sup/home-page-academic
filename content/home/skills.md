---
# An instance of the Featurette widget.
# Documentation: https://wowchemy.com/docs/page-builder/
widget: featurette

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 30

title: Skills
subtitle:

# Showcase personal skills or business features.
# - Add/remove as many `feature` blocks below as you like.
# - For available icons, see: https://wowchemy.com/docs/page-builder/#icons
feature:
  - icon: "python"
    icon_pack: "fab"
    name: "Python"
  - icon: "go"
    icon_pack: "simple-icon"
    name: "Go"
  - icon: "java"
    icon_pack: "fab"
    name: "Java"
  - icon: "kotlin"
    icon_pack: "simple-icon"
    name: "Kotlin"
  - icon: "c"
    icon_pack: "simple-icon"
    name: "C"
  - icon: "cplusplus"
    icon_pack: "simple-icon"
    name: "C++"
  - icon: "cpu"
    icon_pack: "heiafr-icon"
    name: "Embedded systems"
  - icon: "linux"
    icon_pack: "fab"
    name: "Operating Systems"
  - icon: "cloud"
    icon_pack: "fas"
    name: "Cloud"

# Uncomment to use emoji icons.
#- icon: ":smile:"
#  icon_pack: "emoji"
#  name: "Emojiness"
#  description: "100%"

# Uncomment to use custom SVG icons.
# Place your custom SVG icon in `assets/media/icons/`.
# Reference the SVG icon name (without `.svg` extension) in the `icon` field.
# For example, reference `assets/media/icons/xyz.svg` as `icon: 'xyz'`
#- icon: "your-custom-icon-name"
#  icon_pack: "custom"
#  name: "Surfing"
#  description: "90%"
---
