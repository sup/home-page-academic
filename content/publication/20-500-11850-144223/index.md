---
title: "ODÉON. An object-oriented data model and its integration in the Oberon system"
date: 1999-01-01
publishDate: 2020-05-24T22:05:40.475694Z
authors: ["Jacques Supcik"]
publication_types: ["7"]
abstract: ""
featured: true
publication: "*ETH Zürich*"
tags: ["Oberon", "ODÉON"]
doi: "10.3929/ethz-a-003821749"
url_pdf: https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/144223/eth-23075-02.pdf?sequence=2&isAllowed=y
---

